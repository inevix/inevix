/*
INITIALIZATION FUNCTIONS
*/

let windowWidth = window.innerWidth;

jQuery( document ).ready( function( $ ) {

    formPlaceholder();
    formValidation();
    toggleSections();

} );

jQuery( window ).bind( 'resize', function() {
    windowWidth = window.innerWidth;
} );

window.onload = function() {

    scrollLock.hide();
    document.body.classList.remove('is-load');
    loadAnimation();

}

document.addEventListener('DOMContentLoaded', function() {

    toggleBgVolume();
    clickSound();
    toggleFeedback();
    skillProgress();
    toggleMobileProfile();

});

function loadAnimation() {
    let animParticles = document.querySelector('.js-particles'),
        animLogo = document.querySelector('.js-logo'),
        animHeader = document.querySelector('.js-header'),
        animFooter = document.querySelector('.js-footer'),
        animVolumeButton = document.querySelector('.js-toggle-volume'),
        animFeedbackButton = document.querySelector('.js-toggle-feedback');

    animParticles.classList.add('is-visible');

    setTimeout( function() {
        animLogo.classList.add('is-visible');
    }, 2500 );

    setTimeout( function() {
        animHeader.classList.add('is-visible');
        animFooter.classList.add('is-visible');
    }, 3500 );

    // setTimeout( function() {
    //     animPorfile.classList.add('is-visible');
    //     animProjects.classList.add('is-visible');
    // }, 3500 );
    //
    // setTimeout( function() {
    //     animFooter.classList.add('is-visible');
    // }, 4500 );

    setTimeout( function() {
        animVolumeButton.classList.add('is-visible');
        animFeedbackButton.classList.add('is-visible');
    }, 4500 );

}

function toggleBgVolume() {
    let bgSound = document.querySelector('.js-audio'),
        hoverSound = document.querySelector('.js-audio-open'),
        clickSound = document.querySelector('.js-audio-click'),
        volumeButton = document.querySelector('.js-toggle-volume');

    volumeButton.addEventListener('click', function() {
        let tooltip = this.querySelector('span');

        if ( this.classList.contains('is-mute') ) {
            this.classList.remove('is-mute');
            tooltip.innerHTML = 'Mute';
            bgSound.volume = '.5';
            hoverSound.volume = '1';
            clickSound.volume = '1';
        } else {
            this.classList.add('is-mute');
            tooltip.innerHTML = 'Unmute';
            bgSound.volume = '0';
            hoverSound.volume = '0';
            clickSound.volume = '0';
        }

        event.preventDefault();
    });

}

function clickSound() {
    let clickLinks = document.querySelectorAll('.js-click-link'),
        clickSound = document.querySelector('.js-audio-click');

    Array.from(clickLinks).forEach(link => {

        link.addEventListener('click', function() {
            clickSound.play();
        });

    });
}

function toggleFeedback() {
    let feedbackForm = document.querySelector('.js-feedback-form'),
        feedbackButton = document.querySelector('.js-toggle-feedback'),
        volumeButton = document.querySelector('.js-toggle-volume'),
        animLogo = document.querySelector('.js-logo'),
        formInputs = document.querySelectorAll('.js-input'),
        formInputsWithPlaceholder = document.querySelectorAll('.js-has-placeholder'),
        openSound = document.querySelector('.js-audio-open'),
        header = document.querySelector('.js-header'),
        footer = document.querySelector('.js-footer');

    feedbackButton.addEventListener('click', function() {
        let icon = this.querySelector('i'),
            tooltip = this.querySelector('span'),
            errors = document.querySelectorAll('.has-error');

        if ( feedbackForm.classList.contains('is-active') ) {
            this.classList.remove('is-active');
            feedbackForm.classList.remove('is-active');
            icon.classList.add('fa-envelope');
            icon.classList.remove('fa-envelope-open');
            tooltip.innerHTML = 'Open Feedback';

            if ( windowWidth < 768 ) {
                header.classList.add('is-visible');
                footer.classList.add('is-visible');
                feedbackButton.classList.remove('open-feedback');
                volumeButton.classList.remove('open-feedback');
            }

            setTimeout(function () {
                openSound.play();
            }, 1000);

            setTimeout(function () {
                animLogo.classList.add('is-visible');
            }, 1000);

            for ( let i = 0; i < formInputs.length; i++ ) {
                formInputs[i].value = '';
            }

            Array.from(errors).forEach(error => {
                error.parentNode.removeChild(error);
            });

            for ( let i = 0; i < formInputsWithPlaceholder.length; i++ ) {

                if ( formInputsWithPlaceholder[i].parentNode.querySelector('.js-input-placeholder').classList.contains('is-active') ) {
                    formInputsWithPlaceholder[i].parentNode.querySelector('.js-input-placeholder').classList.remove('is-active');
                }

            }

        } else {
            this.classList.add('is-active');
            animLogo.classList.remove('is-visible');
            icon.classList.remove('fa-envelope');
            icon.classList.add('fa-envelope-open');
            tooltip.innerHTML = 'Close Feedback';

            if ( windowWidth < 768 ) {
                header.classList.remove('is-visible');
                footer.classList.remove('is-visible');
                feedbackButton.classList.add('open-feedback');
                volumeButton.classList.add('open-feedback');
            }

            setTimeout(function () {
                feedbackForm.classList.add('is-active');
                openSound.play();
            }, 1000);

        }

        event.preventDefault();
    });
}

function formPlaceholder() {
    let form = $('.js-form'),
        formHolder = $('.js-form-holder'),
        formInput = $('.js-input'),
        inputPlacehodler = $('.js-input-placeholder');

    if ( form.length ) {

        formInput.on( 'focus', function() {
            $(this).closest(formHolder).find(inputPlacehodler).addClass('is-active');
        } );

        formInput.on( 'blur', function() {

            if ( $(this).val() === '' ) {
                $(this).closest(formHolder).find(inputPlacehodler).removeClass('is-active');
            }

        } );

    }
}

function formValidation() {
    let form = $('.js-form'),
        formInput = $('.js-input'),
        formButton = $('.js-submit'),
        errors = document.querySelector('.js-form').getElementsByClassName('has-error'),
        hoverSound = document.querySelector('.js-audio-open'),
        clickSound = document.querySelector('.js-audio-click');

    if ( form.length ) {

        form.on( 'keydown', function( e ) {

            if( e.keyCode === 13 ) e.preventDefault();

        } );

        formInput.on( 'change', function() {

            checkInput( $(this) );

        } );

        formButton.on( 'click', function( e ) {
            e.preventDefault();

            formInput.each( function() {

                checkInput( $(this) );

            } );

            if ( errors.length === 0 ) {
                form.submit();
                formInput.val('');
            }

        } );

        form.submit( function( e ) {
            e.preventDefault();

            let tooltip = $('.js-tooltip');

            let dataForm = {};
            $(this).find(formInput).each( function() {
                dataForm[this.name] = $(this).val();
            } );

            $.ajax( {
                url: '../feedback.php',
                type: 'POST',
                data: $(this).serialize(),
                complete: function() {
                    let errors = document.querySelectorAll('.has-error'),
                        feedbackForm = document.querySelector('.js-feedback-form'),
                        feedbackButton = document.querySelector('.js-toggle-feedback'),
                        formInputs = document.querySelectorAll('.js-input'),
                        formInputsWithPlaceholder = document.querySelectorAll('.js-has-placeholder'),
                        icon = feedbackButton.querySelector('i'),
                        title = feedbackButton.querySelector('span'),
                        logo = document.querySelector('.js-logo');

                    setTimeout( function() {
                        tooltip.addClass('is-active');
                        clickSound.play();
                        logo.classList.add('is-visible');
                    }, 1000 );

                    if ( feedbackForm.classList.contains('is-active') ) {
                        feedbackButton.classList.remove('is-active');
                        feedbackForm.classList.remove('is-active');
                        icon.classList.add('fa-envelope');
                        icon.classList.remove('fa-envelope-open');
                        title.innerHTML = 'Open Feedback';

                        for ( let i = 0; i < formInputs.length; i++ ) {
                            formInputs[i].value = '';
                        }

                        Array.from(errors).forEach(error => {
                            error.parentNode.removeChild(error);
                        });

                        for ( let i = 0; i < formInputsWithPlaceholder.length; i++ ) {

                            if ( formInputsWithPlaceholder[i].parentNode.querySelector('.js-input-placeholder').classList.contains('is-active') ) {
                                formInputsWithPlaceholder[i].parentNode.querySelector('.js-input-placeholder').classList.remove('is-active');
                            }

                        }
                    }

                    setTimeout( function() {
                        tooltip.removeClass('is-active');
                        tooltip.text('');
                        hoverSound.play();
                    }, 8000 );

                    console.log( $(this).serialize() )

                },
                error: function() {
                    tooltip.text('Error sending message!');
                },
                success: function() {
                    tooltip.text('Message successfully send!');
                }
            } );

        } );

    }
}

function checkInput( input ) {
    let form = $('.js-form'),
        formHolder = $('.js-form-holder');

    if ( form.attr('data-validation') === 'true' ) {

        if ( input.attr('data-require') === 'name' ) {
            let errorTextEmpty = 'This field is empty!',
                errorTextLength = 'Please enter more 1 character',
                errorBox = $('.has-error');

            if ( input.val().length === 0 ) {
                input.closest(formHolder).find(errorBox).remove();
                input.closest(formHolder).append(`<span class="has-error">${errorTextEmpty}</span>`);
                input.attr('data-correct', 'false');
            } else if ( input.val().length < 2 ) {
                input.closest(formHolder).find(errorBox).remove();
                input.closest(formHolder).append(`<span class="has-error">${errorTextLength}</span>`);
                input.attr('data-correct', 'false');
            } else {
                input.closest(formHolder).find(errorBox).remove();
                input.attr('data-correct', 'true');
            }

        } else if ( input.attr('data-require') === 'email' ) {
            let errorTextEmpty = 'This field is empty!',
                errorTextIncorrect = 'Incorrect e-mail address',
                errorBox = $('.has-error'),
                correctEmailExample = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                correctEmail = correctEmailExample.test(String(input.val()).toLowerCase());

            if ( input.val().length === 0 ) {
                input.closest(formHolder).find(errorBox).remove();
                input.closest(formHolder).append(`<span class="has-error">${errorTextEmpty}</span>`);
                input.attr('data-correct', 'false');
            } else if ( !input.val() === correctEmail ) {
                input.closest(formHolder).find(errorBox).remove();
                input.closest(formHolder).append(`<span class="has-error">${errorTextIncorrect}</span>`);
                input.attr('data-correct', 'false');
            } else {
                input.closest(formHolder).find(errorBox).remove();
                input.attr('data-correct', 'true');
            }

        } else if ( input.attr('data-require') === 'message' ) {
            let errorTextEmpty = 'This field is empty!',
                errorTextLength = 'Please enter more 20 characters',
                errorBox = $('.has-error');

            if ( input.val().length === 0 ) {
                input.closest(formHolder).find(errorBox).remove();
                input.closest(formHolder).append(`<span class="has-error">${errorTextEmpty}</span>`);
                input.attr('data-correct', 'false');
            } else if ( input.val().length < 20 ) {
                input.closest(formHolder).find(errorBox).remove();
                input.closest(formHolder).append(`<span class="has-error">${errorTextLength}</span>`);
                input.attr('data-correct', 'false');
            } else {
                input.closest(formHolder).find(errorBox).remove()
                input.attr('data-correct', 'true');
            }

        }

    }
}

function toggleSections() {
    let toggleBoxes = $('.js-header, .js-footer, .js-logo, .js-toggle-volume, .js-toggle-feedback'),
        toggleButton = $('.js-toggle-boxes'),
        sections = $('.js-section'),
        sectionsContent = $('.js-section-content'),
        feedbackForm = document.querySelector('.js-feedback-form'),
        feedbackButton = document.querySelector('.js-toggle-feedback'),
        formInputs = document.querySelectorAll('.js-input'),
        formInputsWithPlaceholder = document.querySelectorAll('.js-has-placeholder');

    if ( sections.length ) {

        toggleButton.on( 'click', function( e ) {
            toggleBoxes.removeClass('is-visible');

            sectionsContent.scrollTop(0);

            let icon = feedbackButton.querySelector('i'),
                tooltip = feedbackButton.querySelector('span'),
                errors = document.querySelectorAll('.has-error');

            if ( feedbackForm.classList.contains('is-active') ) {
                feedbackButton.classList.remove('is-active');
                feedbackForm.classList.remove('is-active');
                icon.classList.add('fa-envelope');
                icon.classList.remove('fa-envelope-open');
                tooltip.innerHTML = 'Open Feedback';

                for ( let i = 0; i < formInputs.length; i++ ) {
                    formInputs[i].value = '';
                }

                Array.from(errors).forEach(error => {
                    error.parentNode.removeChild(error);
                });

                for ( let i = 0; i < formInputsWithPlaceholder.length; i++ ) {

                    if ( formInputsWithPlaceholder[i].parentNode.querySelector('.js-input-placeholder').classList.contains('is-active') ) {
                        formInputsWithPlaceholder[i].parentNode.querySelector('.js-input-placeholder').classList.remove('is-active');
                    }

                }
            }

            e.preventDefault();
        } );

        openProfile();
        openProjects();
        closeSections();

    }

}

function openProfile() {
    let openProfile = $('.js-open-profile'),
        boxProfile = $('.js-profile'),
        openSound = document.querySelector('.js-audio-open');

    openProfile.on( 'click', function( e ) {

        setTimeout( function() {
            boxProfile.addClass('is-active');
            openSound.play();
        }, 1500);

        e.preventDefault();
    } );

}

function openProjects() {
    let openProjects = $('.js-open-projects'),
        boxProjects = $('.js-projects'),
        openSound = document.querySelector('.js-audio-open');

    openProjects.on( 'click', function( e ) {

        setTimeout( function() {
            boxProjects.addClass('is-active');
            openSound.play();
        }, 1500);

        e.preventDefault();
    } );

}

function closeSections() {
    let closeSections = $('.js-close-sections'),
        sections = $('.js-section'),
        toggleBoxes = $('.js-header, .js-footer, .js-logo, .js-toggle-volume, .js-toggle-feedback'),
        openSound = document.querySelector('.js-audio-open');

    closeSections.on( 'click', function( e ) {
        sections.removeClass('is-active');

        setTimeout( function() {
            openSound.play();
        }, 1000);

        setTimeout( function() {
            toggleBoxes.addClass('is-visible');
        }, 2500);

        e.preventDefault();
    } );

}

function skillProgress() {
    let barProgress = document.querySelectorAll('.js-progress');

    Array.from(barProgress).forEach(bar => {
       let progressPercent = bar.getAttribute('data-progress');

       bar.style.width = `${progressPercent}%`;
    });
}

function toggleMobileProfile() {
    let toggleButton = document.querySelector('.js-toggle-mobile-profile'),
        container = document.querySelector('.js-profile-container'),
        content = document.querySelector('.js-section-content'),
        openSound = document.querySelector('.js-audio-open');

    toggleButton.addEventListener('click', function() {

        if ( windowWidth < 1025 ) {
            container.classList.toggle('is-active');
            content.classList.toggle('sl--scrollable');
            openSound.play();
        }

    });

    content.addEventListener('click', function() {

        if ( windowWidth < 1025 && container.classList.contains('is-active') ) {
            container.classList.toggle('is-active');
            content.classList.toggle('sl--scrollable');
            openSound.play();
        }

    });

}

particlesJS( 'particles-js',

    {
        "particles": {
            "number": {
                "value": 160,
                "density": {
                    "enable": true,
                    "value_area": 800
                }
            },
            "color": {
                "value": "#ffffff"
            },
            "shape": {
                "type": "circle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                },
                "polygon": {
                    "nb_sides": 5
                }
            },
            "opacity": {
                "value": 1,
                "random": true,
                "anim": {
                    "enable": true,
                    "speed": 1,
                    "opacity_min": 0,
                    "sync": false
                }
            },
            "size": {
                "value": 3,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 4,
                    "size_min": 0.3,
                    "sync": false
                }
            },
            "line_linked": {
                "enable": false,
                "distance": 150,
                "color": "#ffffff",
                "opacity": 0.4,
                "width": 1
            },
            "move": {
                "enable": true,
                "speed": 1,
                "direction": "none",
                "random": true,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 600
                }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": true,
                    "mode": "bubble"
                },
                "onclick": {
                    "enable": true,
                    "mode": "push"
                },
                "resize": true
            },
            "modes": {
                "grab": {
                    "distance": 400,
                    "line_linked": {
                        "opacity": 1
                    }
                },
                "bubble": {
                    "distance": 250,
                    "size": 0,
                    "duration": 2,
                    "opacity": 0,
                    "speed": 3
                },
                "repulse": {
                    "distance": 400,
                    "duration": 0.4
                },
                "push": {
                    "particles_nb": 4
                },
                "remove": {
                    "particles_nb": 2
                }
            }
        },
        "retina_detect": true,
        "config_demo": {
            "hide_card": false,
            "background_color": "#000",
            "background_image": "",
            "background_position": "50% 50%",
            "background_repeat": "no-repeat",
            "background_size": "cover"
        }
    }

);