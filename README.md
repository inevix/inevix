## DEFAULT THEME TEMPLATE FOR WORK

### GENERAL INFO:
1. Toolkit: **Gulp**.
2. Preprocessors: **SASS**, **BABEL**.
3. Frameworks: **none**.
4. JS library: **jQuery**.
5. Support **ES2015+**.

### HOW USE:
1. Open terminal and input: `npm install --save-dev`.
2. Run task: `gulp`.
3. Make all changes in **src/**.
4. Use **https://realfavicongenerator.net/** for generate favicons and move them to **favicons/**.
5. Use **https://icomoon.io/** for generate web-font from svg-icons.
6. If you want to connect a **Bootstrap 4 grid**, then uncomment the comment in **src/layouts/includes/styles.html** `line 1`. 

#### - HTML:
- Edit the home page in **src/index.html**.
- For html-files is used **gulp-rigger**.
- All layouts and includes are in **src/layouts/**.

### - SCSS:
- Make all changes in **src/scss/**.
- Main variables are in **src/scss/_main/**.
- Read comments in **style.scss**, **_config.scss** and **_main-config.scss**.
- Flex-box classes are in **src/scss/reset/_flexbox.scss**.
- All plugins are move to **src/scss/_libs/**.

### - JS:
- Make the main changes in **src/js/main.js**.
- All plugins are move to **src/js/_libs/**.

### - FONTS
- All fonts are move to **src/fonts/**.
- Include font in variable **$used-fonts** in **src/scss/_main/_fonts.scss**.

### - IMAGES
- All images are move to **src/img/**.